@echo off


:define
@set interval=1
setlocal enableextensions enabledelayedexpansion
for /f %%a in ('copy /Z "%~dpf0" nul') do set "CR=%%a"


:header
echo ###################
echo # Schleifen-Timer #
echo #   for andrena   #
echo #    by padhie    #
echo ###################

timeout /t %interval% /nobreak > NUL
<nul set /p"=start in 5 !CR!"
timeout /t %interval% /nobreak > NUL
<nul set /p"=start in 4 !CR!"
timeout /t %interval% /nobreak > NUL
<nul set /p"=start in 3 !CR!"
timeout /t %interval% /nobreak > NUL
<nul set /p"=start in 2 !CR!"
timeout /t %interval% /nobreak > NUL
<nul set /p"=start in 1 !CR!"
timeout /t %interval% /nobreak > NUL


:input
cls
@set /p input_duration=Dauer in Minuten:
echo ---------------------------------------------

@set duration_minutes=%input_duration%
@set duration_seconds=0

echo Timer start with %duration_minutes% Minutes

@set /a "duration=%duration_minutes%*60"
@set /a "duration=%duration%+%duration_seconds%


:counter_loop
if %duration_seconds% gtr 9 (
    <nul set /p"=%duration_minutes%:%duration_seconds% !CR!"
    (echo %duration_minutes%:%duration_seconds%) > output.txt
) else (
    <nul set /p"=%duration_minutes%:0%duration_seconds% !CR!"
    (echo %duration_minutes%:0%duration_seconds%) > output.txt
)

@set /a "duration=%duration%-1
if "%duration_seconds%"=="0" (
    @set /a "duration_minutes=%duration_minutes%-%interval%"
    @set /a "duration_seconds=59"
) else (
    @set /a "duration_seconds=%duration_seconds%-%interval%"
)

timeout /t %interval% /nobreak > NUL

if %duration% gtr 0 (
    GOTO counter_loop
)


:outro
<nul set /p"=done"
echo 

if exist "sound.mp3" (
    start sound.mp3
) else if exist "sound.wav" (
    start sound.wav
)

<nul set /p"=restart in 10 !CR!"
(echo done) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 9 !CR!"
(break) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 8 !CR!"
(echo done) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 7 !CR!"
(break) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 6 !CR!"
(echo done) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 5 !CR!"
(break) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 4 !CR!"
(echo done) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 3 !CR!"
(break) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 2 !CR!"
(echo done) > output.txt
timeout /t %interval% /nobreak > NUL

<nul set /p"=restart in 1 !CR!"
(break) > output.txt
timeout /t %interval% /nobreak > NUL


:end
GOTO input