# hair bow timer

This script is for the streamer [andrenagurke](https://www.twitch.tv/andrenagurke).  
It is just a timer for the hair bow as stream feature.


## for stream
The script create in `output.txt` file with the current time.  
At the end the file content will be set and replace `done` for a blink effect.


## notification
At the end of the time the system `beep` sound will be played.  
If you put a `sound.mp3` or `sound.wav` near the script, this will be play at the end too. It will be use your default system media player.
